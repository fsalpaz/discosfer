﻿using DiscosFer.DataAccessLayer;
using DiscosFer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DiscosFer.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CreateCustomer()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateCustomer(Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                using (var context = new DiscosDAL())
                {
                    context.Clientes.Add(cliente);
                    context.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            else
                return View();
        }
    }
}