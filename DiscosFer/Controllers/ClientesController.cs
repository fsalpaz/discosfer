﻿using DiscosFer.DataAccessLayer;
using DiscosFer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DiscosFer.Controllers
{
    public class ClientesController : Controller
    {
        // GET: Clientes
        public ActionResult Index()
        {
            return RedirectToAction("GetClientes");
        }

        public ActionResult GetClientes()
        {
            List<Cliente> clientes;

            using (var context = new DiscosDAL())
            {
                clientes = context.Clientes.ToList();
            }
            return View(clientes);
        }

        public ActionResult GetPuntuaciones(int id)
        {
            List<Puntuacion> puntuaciones;
            using (var context = new DiscosDAL())
            {
                puntuaciones = context.Puntuaciones.Where(p => p.Idcliente == id).ToList();
                return View(puntuaciones);
            }
        }
    }
}