﻿using DiscosFer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DiscosFer.DataAccessLayer
{
    public class DiscosDAL : DbContext
    {
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Disco> Discos { get; set; }
        public DbSet<Puntuacion> Puntuaciones { get; set; }
        public DbSet<Interprete> Interpretes { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>().ToTable("Cliente");
            modelBuilder.Entity<Disco>().ToTable("Disco");
            modelBuilder.Entity<Puntuacion>().ToTable("Puntuacion");
            modelBuilder.Entity<Interprete>().ToTable("Interprete");

            base.OnModelCreating(modelBuilder);
        }


    }
}