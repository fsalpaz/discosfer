﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DiscosFer.Models
{
    public class Cliente
    {
        public Cliente()
        {
            this.Puntuaciones = new HashSet<Puntuacion>();
        }

        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "Nombre requerido")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "EMail requerido")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Fecha nacimiento incorrecta")]
        public Nullable<System.DateTime> FechaNacimiento { get; set; }

        [Required(ErrorMessage = "Fecha de registro requerida")]
        [DataType(DataType.Date, ErrorMessage = "Fecha registro incorrecta")]
        public Nullable<System.DateTime> FechaRegistro { get; set; }

        public virtual ICollection<Puntuacion> Puntuaciones { get; set; }
    }
}