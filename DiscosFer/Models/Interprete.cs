﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DiscosFer.Models
{
    public class Interprete
    {
        public Interprete()
        {
            this.Discos = new HashSet<Disco>();
        }

        [Key]
        public int IdInterprete { get; set; }

        public string Interprete1 { get; set; }

        public virtual ICollection<Disco> Discos { get; set; }
    }
}