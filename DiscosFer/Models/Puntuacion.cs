﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DiscosFer.Models
{
    public class Puntuacion
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> Idcliente { get; set; }
        public Nullable<int> iddisco { get; set; }
        [Column("Puntuacion")]
        public Nullable<int> PuntuacionVal { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }

        [ForeignKey("Idcliente")]
        public virtual Cliente Cliente { get; set; }

        [ForeignKey("iddisco")]
        public virtual Disco Disco { get; set; }
    }
}